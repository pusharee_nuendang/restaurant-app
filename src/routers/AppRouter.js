import React from 'react';
import { BrowserRouter, Route, Switch} from 'react-router-dom';
import LandingPage from '../components/LandingPage';
import PromotionPage from '../components/PromotionPage';
import PromotionAdd from '../components/PromotionAdd';
import PromotionEdit from '../components/PromotionEdit';
import ReservationPage from '../components/ReservationPage';
import ReservationAdd from '../components/ReservationAdd';
import ReservationEdit from '../components/ReservationEdit';
import BillCalculatePage from '../components/BillCalculatePage';
import BillList from '../components/BillList';
import NotFoundPage from '../components/NotFoundPage';
import PublicRoute from './PublicRouter';
const AppRouter = () => (
  <BrowserRouter>
    <div>
      <Switch>
        <PublicRoute path="/" component={BillList} exact={true} />
        <PublicRoute path="/list" component={BillList} />
        <PublicRoute path="/calculator" component={BillCalculatePage} />
        <PublicRoute path="/promotion" component={PromotionPage} />
        <PublicRoute path="/promotion-add" component={PromotionAdd} />
        <PublicRoute path="/promotion-edit/:id" component={PromotionEdit} />
        <PublicRoute path="/reservation" component={ReservationPage} />
        <PublicRoute path="/reservation-add" component={ReservationAdd} />
        <PublicRoute path="/reservation-edit/:id" component={ReservationEdit} />
        
        <Route component={NotFoundPage} />
      </Switch>
      
    </div>
  </BrowserRouter>
);

export default AppRouter;