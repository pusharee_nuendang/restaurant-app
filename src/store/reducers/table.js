
const dataDefault =
    [
        {
            id: 1,
            title: "Table#1 / 1 seat (Zone 1)",
            seat: 1,
            zone: 1
        },
        {
            id: 2,
            title: "Table#2 / 1 seat (Zone 1)",
            seat: 1,
            zone: 1
        },
        {
            id: 3,
            title: "Table#3 / 1 seats (Zone 1)",
            seat: 1,
            zone: 1
        },
        {
            id: 4,
            title: "Table#4 / 1 seat (Zone 1)",
            seat: 1,
            zone: 1
        },
        {
            id: 5,
            title: "Table#5 / 1 seat (Zone 1)",
            seat: 1,
            zone: 1
        },
        {
            id: 6,
            title: "Table#6 / 1 seat (Zone 1)",
            seat: 1,
            zone: 1
        },
        {
            id: 7,
            title: "Table#7 / 1 seat (Zone 1)",
            seat: 1,
            zone: 1
        },
        {
            id: 8,
            title: "Table#8 / 1 seat (Zone 1)",
            seat: 1,
            zone: 1
        },
        {
            id: 9,
            title: "Table#9 / 1 seat (Zone 1)",
            seat: 1,
            zone: 1
        },
        {
            id: 10,
            title: "Table#10 / 1 seat (Zone 1)",
            seat: 1,
            zone: 1
        },
        {
            id: 11,
            title: "Table#11 / 1 seat (Zone 1)",
            seat: 1,
            zone: 1
        },
        {
            id: 12,
            title: "Table#12 / 1 seat (Zone 1)",
            seat: 1,
            zone: 1
        },

        {
            id: 13,
            title: "Table#13 / 8 seats (Zone 2)",
            seat: 8,
            zone: 2
        },
        {
            id: 14,
            title: "Table#14 / 8 seats (Zone 2)",
            seat: 8,
            zone: 2

        },
        {
            id: 15,
            title: "Table#15 / 4 seats (Zone 2)",
            seat: 6,
            zone: 2
        },
        {
            id: 16,
            title: "Table#16 / 4 seats (Zone 2)",
            seat: 6,
            zone: 2

        },
        {
            id: 17,
            title: "Table#17 / 4 seats (Zone 2) ",
            seat: 6,
            zone: 2

        },
        {
            id: 18,
            title: "Table#18 / 4 seats (Zone 2)",
            seat: 6,
            zone: 2

        },
        {
            id: 19,
            title: "Table#19 / 4 seats (Zone 2)",
            seat: 6,
            zone: 2

        },
        {
            id: 20,
            title: "Table#20 / 4 seats (Zone 2)",
            seat: 6,
            zone: 2

        },
        {
            id: 21,
            title: "Table#21 / 2 seats (Zone 2)",
            seat: 2,
            zone: 2

        },
        {
            id: 22,
            title: "Table#22 / 2 seats (Zone 2)",
            seat: 2,
            zone: 2

        },
        {
            id: 23,
            title: "Table#23 / 2 seats (Zone 2)",
            seat: 2,
            zone: 2

        },
        {
            id: 24,
            title: "Table#24 / 2 seats (Zone 2)",
            seat: 2,
            zone: 2

        },

    ]

const tableReducer = (state = dataDefault, action) => {
    switch (action.type) {
        default:
            return state
    }
}

export default tableReducer