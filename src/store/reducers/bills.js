
const dataDefault =
    [
        {
            id: 1,
            is_promotion: 0,
            buffet_price: 459,
            customer: 1,
            coupon_code: '',
            status: '1',
            discount: 0,
            subtotal: 459,
            total: 459,
            created_date:""
        },  
        {
            id: 2,
            is_promotion: 0,
            buffet_price: 459,
            customer: 1,
            coupon_code: '',
            status: '1',
            discount: 0,
            subtotal: 459,
            total: 459,
            created_date:""
        }, 
       
      ]


const billReducer = (state = dataDefault, action) => {
    switch(action.type) {
      case 'ADD':
        return [...state, action.payload]
        
      case 'EDIT':
          const updatedItems = state.map(item => {
            if(item.id === action.id){
              return { ...item, ...action.payload }
            }
            return item
          })
          return updatedItems
      case 'REMOVE':
        return state.filter((item, index) => index !== action.payload)
  
      default:
        return state
    }
  }
  
  export default billReducer