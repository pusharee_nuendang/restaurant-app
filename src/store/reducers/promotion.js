
const promotions =
    [
        {
          id: 1,
          coupon_code_type:1,
          coupon_code: 'LUCKY ONE',
          percentage: 15,
          over_price: 1000,
          get: 0,
          free: 0,
          created_date: '',
          updated_date:''
        },  
        {
            id: 2,
            coupon_code_type:2,
            coupon_code: '4PAY3',
            percentage: 0,
            over_price: 0,
            get: 4,
            free: 1,
            created_date: '',
            updated_date:''
          },
        {
          id: 3,
          coupon_code_type:1,
          coupon_code: 'LUCKY TWO',
          percentage: 20,
          over_price: 0,
          get: 2,
          free: 0,
          created_date: '',
          updated_date:''
        },
       
      ]


const listReducer = (state = promotions, action) => {
    switch(action.type) {
      case 'ADD_PROMOTION':
        return [...state, action.payload]
        
      case 'EDIT_PROMOTION':
          const updatedItems = state.map(item => {
            if(item.id === action.id){
              return { ...item, ...action.payload }
            }
            return item
          })
          return updatedItems
      case 'REMOVE_PROMOTION':
        return state.filter((item, index) => item.id !== action.payload)
  
      default:
        return state
    }
  }
  
  export default listReducer