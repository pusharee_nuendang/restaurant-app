
const dataDefault =
    [
        {
          id: 1,
          customer: "8",
          email: "patcharanan@gmail.com",
          name: "patcharanan",
          reserv_date: "2019-04-3",
          tables: [{id: 14, title: "Table#14 8 seats (Zone 2)", seat: 8, zone: 2}],
          tel: "0918719822",
          created_date: "2019-03-23T06:19:15.946Z",
          updated_date: "2019-03-23T06:19:15.946Z",
        },  
        {
            id: 2,
            customer: "4",
            email: "bordin@gmail.com",
            name: "bordin",
            reserv_date: "2019-04-5",
            tables: [{id: 15, title: "Table#15 4 seats (Zone 2)", seat: 6, zone: 2}],
            tel: "0918719822",
            created_date: "2019-03-23T06:19:15.946Z",
            updated_date: "2019-03-23T06:19:15.946Z",
          }, 
          {
          id: 3,
          created_date: "2019-03-23T06:19:15.946Z",
          customer: "2",
          email: "misspusharee@gmail.com",
          name: "padchatee",
          reserv_date: "2019-04-5",
          tables: [ 
            {id: 1, title: "Table#1 / 1 seat (Zone 1)", seat: 1, zone: 1},
            {id: 2, title: "Table#2 / 1 seat (Zone 1)", seat: 1, zone: 1}],
          tel: "0918719822",
          updated_date: "2019-03-23T06:19:15.946Z",
          }, 
      ]


const reservtionReducer = (state = dataDefault, action) => {
    switch(action.type) {
      case 'ADD_RESERVATION':
        return [...state, action.payload]
        
      case 'EDIT_RESERVATION':
          const updatedItems = state.map(item => {
            if(item.id === action.id){
              return { ...item, ...action.payload }
            }
            return item
          })
          return updatedItems
      case 'REMOVE_RESERVATION':
        return state.filter((item, index) => item.id !== action.payload)
  
      default:
        return state
    }
  }
  
  export default reservtionReducer