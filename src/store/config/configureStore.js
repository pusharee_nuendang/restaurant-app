import { createStore, combineReducers ,applyMiddleware, compose  } from 'redux';
import authReducer from '../reducers/auth';
import listReducer from '../reducers/promotion';
import reservtionReducer from '../reducers/reservation';
import billReducer from '../reducers/bills';
import tableReducer from '../reducers/table';


function saveToLocalStorage(state) {
  try {
    const serializedState = JSON.stringify(state)
    localStorage.setItem('state', serializedState)
  } catch(e) {
    console.log(e)
  }
}

function loadFromLocalStorage() {
  try {
    const serializedState = localStorage.getItem('state')
    if (serializedState === null) return undefined
    return JSON.parse(serializedState)
  } catch(e) {
    console.log(e)
    return undefined
  }
}

const rootReducer = combineReducers({
  list: listReducer,
  auth : authReducer,
  reservtionList: reservtionReducer,
  billList : billReducer,
  tableList:tableReducer
})
const persistedState = loadFromLocalStorage()

const store = createStore(
  rootReducer,
  persistedState,
  window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__()
)

store.subscribe(() => saveToLocalStorage(store.getState()))

export default store



