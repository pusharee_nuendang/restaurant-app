import React from 'react'
import { connect } from 'react-redux'

class LandingPage extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      newItem: ''
    }

    this.handleChange = this.handleChange.bind(this)
  }
  handleChange(e) {
    this.setState({
      newItem: e.target.value
    })
  }

  render() {
    return (
      <div>
        Hello
      </div>
    )
  }
}

function mapStateToProps(state, ownProps) {
  return {
    list: state.list,
  }
}

function mapDispatchToProps(dispatch) {
  return {
    add: (value) => {
      dispatch({ type: 'ADD', payload: value })
    },
    remove: (index) => {
      dispatch({ type: 'REMOVE', payload: index }) 
    }
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(LandingPage)