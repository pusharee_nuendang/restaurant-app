import React from 'React';
import { connect } from 'react-redux';
import { compose } from "recompose";
import PropTypes from 'prop-types';
import Paper from '@material-ui/core/Paper';
import TextField from '@material-ui/core/TextField';
import Grid from '@material-ui/core/Grid';
import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';
import InputAdornment from '@material-ui/core/InputAdornment';
import Select from '@material-ui/core/Select';
import FormControl from '@material-ui/core/FormControl';
import InputLabel from '@material-ui/core/InputLabel';
import MenuItem from '@material-ui/core/MenuItem';
import { Link } from "react-router-dom";
import classNames from 'classnames';
import CheckCircleIcon from '@material-ui/icons/CheckCircle';
import IconButton from '@material-ui/core/IconButton';
import CloseIcon from '@material-ui/icons/Close';
import ErrorIcon from '@material-ui/icons/Error';
import InfoIcon from '@material-ui/icons/Info';
import green from '@material-ui/core/colors/green';
import amber from '@material-ui/core/colors/amber';
import SnackbarContent from '@material-ui/core/SnackbarContent';
import WarningIcon from '@material-ui/icons/Warning';
import { withStyles } from '@material-ui/core/styles';

const variantIcon = {
  success: CheckCircleIcon,
  warning: WarningIcon,
  error: ErrorIcon,
  info: InfoIcon,
};

const styles1 = theme => ({
  success: {
    backgroundColor: green[600],
  },
  error: {
    backgroundColor: theme.palette.error.dark,
  },
  info: {
    backgroundColor: theme.palette.primary.dark,
  },
  warning: {
    backgroundColor: amber[700],
  },
  icon: {
    fontSize: 20,
  },
  iconVariant: {
    opacity: 0.9,
    marginRight: theme.spacing.unit,
  },
  message: {
    display: 'flex',
    alignItems: 'center',
  },
});

function MySnackbarContent(props) {
  const { classes, className, message, onClose, variant, ...other } = props;
  const Icon = variantIcon[variant];
  return (
    <SnackbarContent
      className={classNames(classes[variant], className)}
      aria-describedby="client-snackbar"
      message={
        <span id="client-snackbar" className={classes.message}>
          <Icon className={classNames(classes.icon, classes.iconVariant)} />
          {message}
        </span>
      }
      action={[
        <IconButton
          key="close"
          aria-label="Close"
          color="inherit"
          className={classes.close}
          onClick={onClose}
        >
        <CloseIcon className={classes.icon} />
        </IconButton>,
      ]}
      {...other}
    />
  );
}

MySnackbarContent.propTypes = {
  classes: PropTypes.object.isRequired,
  className: PropTypes.string,
  message: PropTypes.node,
  onClose: PropTypes.func,
  variant: PropTypes.oneOf(['success', 'warning', 'error', 'info']).isRequired,
};

const MySnackbarContentWrapper = withStyles(styles1)(MySnackbarContent);

  const styles2 = theme => ({
    margin: {
      margin: theme.spacing.unit,
    },
  });

export class PromotionAdd extends React.Component {
  constructor(props) {
    super(props);
    this.max_id = this.props.list.length;
    this.state = {
      messageopen: false,
      open: false,
      messageInfo: {},
      dataForm: {
      id: 1,
      coupon_code_type: 1,
      coupon_code: '',
      percentage: 0,
      over_price: 0,
      get: 0,
      free: 0,
      }
    }
  }
  validate() {
   
    if(this.state.dataForm.coupon_code.trim()==""){
      this.giveErrorMessage('Please specify coupon code');
      return false;
    }
    return true;
  }
  onSubmit = () => {
    if(this.validate()) {
        this.state.dataForm.id = this.max_id + 1
        console.log(this.props.list);
 
          var find = this.props.list.filter(x => x.coupon_code == this.state.dataForm.coupon_code);
          console.log(find);
          if(find.length>0){
              this.giveErrorMessage('Duplicate coupon code');
              return;
          }
        this.props.add(this.state.dataForm)
        this.giveSuccessMessage('Saved');
        setTimeout(
          function () {
            this.props.history.push('/promotion');
          }
            .bind(this),
          3000
        );
    }
    return false;
  }
  giveSuccessMessage = (message) => {
    let newmsg = {
      message,
      key: new Date().getTime()
    };
    this.setState({ messageInfo: newmsg, open: 1 });
    setTimeout(
      function () {
        this.setState({ open: 0 });
      }
        .bind(this),
      2000
    );
  };
  giveErrorMessage = (message) => {
    let newmsg = {
      message,
      key: new Date().getTime()
    };
    this.setState({ messageInfo: newmsg, open: 2 });
    setTimeout(
      function () {
        this.setState({ open: 0 });
      }
        .bind(this),
      2000
    );

  };
  handleChange = name => event => {
    let dataForm = Object.assign({}, this.state.dataForm);    //creating copy of object
    dataForm[name] = event.target.value;
    this.setState({ dataForm })
  };
  render() {
    const { classes } = this.props;
    const { message, key } = this.state.messageInfo;
    return (
      <div className="contact-page-wrapper" justify="center">

        <Grid container spacing={24} justify="center" style={{ marginTop: 10 }}>
          <Grid item xs={8} style={{ margin: 0 }}>
            <Paper className="homepagepaper">
              <div className="headerForm">
                <Typography component="h2">
                  ADD FORM : COUPON CODE
              </Typography>
              </div>
              <Grid item xs={12} lg={12} md={12}>
                <TextField
                  required
                  id="coupon_code"
                  fullWidth={true}
                  label="Coupon code"
                  placeholder="Coupon Code"
                  value={this.state.dataForm.coupon_code}
                  onChange={this.handleChange('coupon_code')}
                  margin="normal"
                />

              </Grid>
              <Grid item xs={12} lg={12} md={12} style={{ marginTop: 10 }} >
                <FormControl fullWidth={true}>
                  <InputLabel htmlFor="age-simple">Discount Type</InputLabel>
                  <Select
                  
                    value={this.state.dataForm.coupon_code_type}
                    onChange={this.handleChange('coupon_code_type')}
                    inputProps={{
                      name: 'coupon_code_type',
                      id: 'coupon_code_type',
                    }}
                    fullWidth={true}
                  >
                    <MenuItem value={1}>Percentage</MenuItem>
                    <MenuItem value={2}>Buy X Get Y For Free</MenuItem>
                  </Select>
                </FormControl>
              </Grid>
              {this.state.dataForm.coupon_code_type == 1 ? <Grid item xs={12} lg={12} md={12} style={{ marginTop: 20 }}>
                <TextField
                
                  id="outlined-adornment-amount"
                  label="Percentage"
                  value={this.state.dataForm.percentage}
                  InputProps={{
                    startAdornment: <InputAdornment position="start">$</InputAdornment>,
                  }}
                  fullWidth={true}
                  onChange={this.handleChange('percentage')}
                />
              </Grid> : null}

              <Grid item xs={12} lg={12} md={12} style={{ marginTop: 20 }}>

                <TextField
                  id="outlined-adornment-amount"
                  label="Over price"
                  value={this.state.dataForm.over_price}
                  InputProps={{
                    startAdornment: <InputAdornment position="start">$</InputAdornment>,
                  }}
                  fullWidth={true}
                  onChange={this.handleChange('over_price')}
                />
              </Grid>
              <Grid item xs={12} lg={12} md={12} style={{ marginTop: 20 }}>

                <TextField
                  id="outlined-adornment-amount"
                  label="Get"
                  value={this.state.dataForm.get}
                  InputProps={{
                    startAdornment: <InputAdornment position="start">$</InputAdornment>,
                  }}
                  fullWidth={true}
                  onChange={this.handleChange('get')}
                />
              </Grid>
              {this.state.dataForm.coupon_code_type == 2 ?
                <Grid item xs={12} lg={12} md={12} style={{ marginTop: 20 }}>
                  <TextField
                    id="outlined-adornment-amount"
                    label="Free"
                    value={this.state.dataForm.free}
                    InputProps={{
                      startAdornment: <InputAdornment position="start">$</InputAdornment>,
                    }}
                    fullWidth={true}
                    onChange={this.handleChange('free')}
                  />
                </Grid> : null}

                <Grid item xs={8} style={{ margin: 0 }}>
                  {this.state.open == 2 ?
                    <MySnackbarContentWrapper
                      variant="error"
                      className={classes.margin}
                      message={message}
                    />
                    : null}
                  {this.state.open == 1 ?
                    <MySnackbarContentWrapper
                      variant="success"
                      className={classes.margin}
                      message={message}
                    /> : null}
                </Grid>
                {this.state.open == 0 ?
                <Grid item xs={12} lg={12} md={12} style={{ marginTop: 40 }}>
                  <Link to="/promotion">
                      <Button type="button" color="default" variant="raised" style={{ marginRight: 10 }} >Go Back</Button>
                  </Link>
                  <Button type="button" color="primary" variant="raised" onClick={this.onSubmit}>Save</Button>
                </Grid>
                 : null}
            </Paper>
          </Grid>
        </Grid>
      </div>
    );
  }

}

function mapStateToProps(state, ownProps) {
  return {
    list: state.list,
  }
}

function mapDispatchToProps(dispatch) {
  return {
    add: (value) => {
      dispatch({ type: 'ADD_PROMOTION', payload: value })
    },

  }
}
PromotionAdd.propTypes = {
  classes: PropTypes.object.isRequired,
};
export default compose(
  withStyles(styles2),
  connect(
    mapStateToProps,
    mapDispatchToProps
  )
)(PromotionAdd);
