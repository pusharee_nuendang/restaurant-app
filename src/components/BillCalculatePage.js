import React from 'React';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import Paper from '@material-ui/core/Paper';
import TextField from '@material-ui/core/TextField';
import Grid from '@material-ui/core/Grid';
import Button from '@material-ui/core/Button';
import IconButton from '@material-ui/core/IconButton';
import CloseIcon from '@material-ui/icons/Close';
import Typography from '@material-ui/core/Typography';
import InputAdornment from '@material-ui/core/InputAdornment';
import { Link } from "react-router-dom";
import classNames from 'classnames';
import CheckCircleIcon from '@material-ui/icons/CheckCircle';
import ErrorIcon from '@material-ui/icons/Error';
import InfoIcon from '@material-ui/icons/Info';
import green from '@material-ui/core/colors/green';
import amber from '@material-ui/core/colors/amber';
import SnackbarContent from '@material-ui/core/SnackbarContent';
import WarningIcon from '@material-ui/icons/Warning';
import { withStyles } from '@material-ui/core/styles';
import { compose } from "recompose";
const variantIcon = {
  success: CheckCircleIcon,
  warning: WarningIcon,
  error: ErrorIcon,
  info: InfoIcon,
};

const styles1 = theme => ({
  success: {
    backgroundColor: green[600],
  },
  error: {
    backgroundColor: theme.palette.error.dark,
  },
  info: {
    backgroundColor: theme.palette.primary.dark,
  },
  warning: {
    backgroundColor: amber[700],
  },
  icon: {
    fontSize: 20,
  },
  iconVariant: {
    opacity: 0.9,
    marginRight: theme.spacing.unit,
  },
  message: {
    display: 'flex',
    alignItems: 'center',
  },
});

function MySnackbarContent(props) {
  const { classes, className, message, onClose, variant, ...other } = props;
  const Icon = variantIcon[variant];

  return (
    <SnackbarContent
      className={classNames(classes[variant], className)}
      aria-describedby="client-snackbar"
      message={
        <span id="client-snackbar" className={classes.message}>
          <Icon className={classNames(classes.icon, classes.iconVariant)} />
          {message}
        </span>
      }
      action={[
        <IconButton
          key="close"
          aria-label="Close"
          color="inherit"
          className={classes.close}
          onClick={onClose}
        >
          <CloseIcon className={classes.icon} />
        </IconButton>,
      ]}
      {...other}
    />
  );
}

MySnackbarContent.propTypes = {
  classes: PropTypes.object.isRequired,
  className: PropTypes.string,
  message: PropTypes.node,
  onClose: PropTypes.func,
  variant: PropTypes.oneOf(['success', 'warning', 'error', 'info']).isRequired,
};

const MySnackbarContentWrapper = withStyles(styles1)(MySnackbarContent);
const styles2 = theme => ({
  margin: {
    margin: theme.spacing.unit,
  },
});
export class BillCalculatePage extends React.Component {
  constructor(props) {
    super(props);
    this.max_id = this.props.billList.length;
    this.state = {
      messageopen: false,
      open: false,
      messageInfo: {},
      dataForm: {
        is_promotion: 0,
        buffet_price: 459,
        customer: 1,
        coupon_code: '',
        status: '1',
        discount: 0,
        subtotal: 459,
        total: 459,
        created_date: ''
      },
      isLoading: true,
      promotions: this.props.list,
      promotions_over_price: {
        percentage: 25,
        over_price: 6000,

      }
    }
  }

  giveSuccessMessage = (message) => {
    let newmsg = {
      message,
      key: new Date().getTime()
    };
    this.setState({ messageInfo: newmsg, open: 1 });
    setTimeout(
      function () {
        this.setState({ open: 0 });
      }
        .bind(this),
      2000
    );
  };
  giveErrorMessage = (message) => {
    let newmsg = {
      message,
      key: new Date().getTime()
    };
    this.setState({ messageInfo: newmsg, open: 2 });
    setTimeout(
      function () {
        this.setState({ open: 0 });
      }
        .bind(this),
      2000
    );

  };
  onApply = () => {
    let dataForm = Object.assign({}, this.state.dataForm);    //creating copy of object
    var promotions = this.state.promotions;
    if( dataForm.is_promotion == 1 ){
      dataForm.coupon_code ='';
      this.setState({ dataForm });
      this.giveErrorMessage('Applied,25% discount from the bill over 6000 ');
      return;
    }
    if (dataForm.coupon_code == '') {
      this.giveErrorMessage('Please specify coupon code');
      return;
    }
    var subtotal = dataForm.subtotal;
    var customer = dataForm.customer * 1;
    if (dataForm.coupon_code != '' && dataForm.is_promotion == 0) {
      var promotion = promotions.find(item => item.coupon_code === dataForm.coupon_code && item.over_price < subtotal && item.get <= customer);
      console.log("promotion", promotion);
      if (promotion) {
        
        if (promotion.coupon_code_type == 1) {
          dataForm.discount = dataForm.subtotal * promotion.percentage / 100;
        } else {
          dataForm.discount = dataForm.buffet_price * promotion.free;
        }
        dataForm.total = dataForm.subtotal - dataForm.discount;
        console.log(dataForm.discount);
        this.giveSuccessMessage('apply Coupon code successfully');
      }else {
        dataForm.coupon_code ='';
        this.giveErrorMessage(dataForm.coupon_code + ' is not a valid code.');
      }
    }
    this.setState({ dataForm });

  };
  onSubmit = () => {
    if(this.validate()){
      this.state.dataForm.created_date = new Date(),
      this.state.dataForm.id = this.max_id + 1
      this.props.add(this.state.dataForm)
      this.giveSuccessMessage(' Saved ');
      setTimeout(
        function () {
          this.props.history.push('/list');
        }
          .bind(this),
        3000
      );
    }
  }
  validate() {
    if(this.state.dataForm.buffet_price<=0){
      this.giveErrorMessage('Please specify price');
      return false;
    }
    if(this.state.dataForm.Customer<=0){
      this.giveErrorMessage('Please specify customer');
      return false;
    }
    return true;
  }

  handleClose = (event, reason) => {
    console.log(1);
    if (reason === 'clickaway') {
      return;
    }
    this.setState({ messageopen: false });
    this.setState({ open: false });
  };

  handleChange = name => event => {

    let dataForm = Object.assign({}, this.state.dataForm);    //creating copy of object
    dataForm[name] = event.target.value;
    dataForm.discount = 0;
    dataForm.subtotal = dataForm.buffet_price * dataForm.customer;
    dataForm.total = dataForm.buffet_price * dataForm.customer;
    if (dataForm.subtotal > this.state.promotions_over_price.over_price) {
      dataForm.discount = dataForm.subtotal * this.state.promotions_over_price.percentage / 100;
      dataForm.total = dataForm.subtotal - dataForm.discount;
      dataForm.is_promotion = 1;
    }

    this.setState({ dataForm });
  };


  render() {
    const { classes } = this.props;
    const { message, key } = this.state.messageInfo;
    return (
      <div className="contact-page-wrapper" justify="center">
        <Grid container spacing={24} justify="center" style={{ marginTop: 10 }}>
          
          <Grid item xs={8} style={{ margin: 0 }}>
            <Paper className="homepagepaper">

              <div className="headerForm">
                <Typography component="h2">
                 BILL CALCULATOR
              </Typography>
              </div>
              <Grid item xs={12} lg={12} md={12}>
                <TextField
                  id="price"
                  fullWidth={true}
                  label="Price"
                  placeholder="Price"
                  value={this.state.dataForm.buffet_price}
                  onChange={this.handleChange('buffet_price')}
                  margin="normal"
                />

              </Grid>

              <Grid item xs={12} lg={12} md={12} style={{ marginTop: 10 }}>
                <TextField
                  id="customer"
                  label="Customer"
                  placeholder="Customer"
                  value={this.state.dataForm.customer}
                  fullWidth={true}
                  type="number"
                  onChange={this.handleChange('customer')}
                  margin="normal"
                />
              </Grid>
              <Grid item xs={8} lg={8} md={8} style={{ marginTop: 10 }}>
                <TextField
                  id="coupon_code"
                  label="Coupon code"
                  placeholder="Coupon code"
                  value={this.state.dataForm.coupon_code}
                  onChange={this.handleChange('coupon_code')}
                  margin="normal"
                />
                <Button type="button" color="primary" variant="raised" onClick={this.onApply} style={{ marginTop: -5 }}>Apply</Button>

              </Grid>
              <Grid item xs={8} lg={8} md={8} style={{ marginTop: 20 }}>

                <TextField
                  id="outlined-adornment-amount"
                  label="Subsubtotal"
                  value={this.state.dataForm.subtotal}
                  InputProps={{
                    startAdornment: <InputAdornment position="start">$</InputAdornment>,
                    readOnly: true,
                  }}
                />
              </Grid>
              <Grid item xs={8} lg={8} md={8} style={{ marginTop: 20 }}>

                <TextField
                  id="outlined-adornment-amount"
                  label="Discount"
                  value={this.state.dataForm.discount}
                  InputProps={{
                    startAdornment: <InputAdornment position="start">$</InputAdornment>,
                    readOnly: true,
                  }}
                />
              </Grid>
              <Grid item xs={8} lg={8} md={8} style={{ marginTop: 20 }}>

                <TextField
                  id="outlined-adornment-amount"
                  label="subtotal"
                  value={this.state.dataForm.total}
                  InputProps={{
                    startAdornment: <InputAdornment position="start">$</InputAdornment>,
                    readOnly: true,
                  }}
                  
                />
              </Grid>

              <Grid item xs={8} style={{ margin: 0 }}>
                  {this.state.open == 2 ?
                    <MySnackbarContentWrapper
                      variant="error"
                      className={classes.margin}
                      message={message}
                    />
                    : null}
                  {this.state.open == 1 ?
                    <MySnackbarContentWrapper
                      variant="success"
                      className={classes.margin}
                      message={message}
                    /> : null}
                </Grid>
                {this.state.open == 0 ?
                <Grid item xs={12} lg={12} md={12} style={{ marginTop: 40 }}>
                  <Link to="/list">
                      <Button type="button" color="default" variant="raised" style={{ marginRight: 10 }} >Go Back</Button>
                  </Link>
                  <Button type="button" color="primary" variant="raised" onClick={this.onSubmit}>Save</Button>
                </Grid>
                 : null}
           
            </Paper>
          </Grid>
        </Grid>
      </div>
    );
  }

}

function mapStateToProps(state, ownProps) {
  return {
    list: state.list,
    billList: state.billList
  }
}

function mapDispatchToProps(dispatch) {
  return {
    add: (value) => {
      dispatch({ type: 'ADD', payload: value })
    }
  }
}

BillCalculatePage.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default compose(
  withStyles(styles2),
  connect(
    mapStateToProps,
    mapDispatchToProps
  )
)(BillCalculatePage);