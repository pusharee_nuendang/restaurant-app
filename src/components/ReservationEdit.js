import 'date-fns';
import React from 'React';
import { connect } from 'react-redux';
import Paper from '@material-ui/core/Paper';
import { withStyles } from "@material-ui/core/styles";
import TextField from '@material-ui/core/TextField';
import Grid from '@material-ui/core/Grid';
import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';
import { Link } from "react-router-dom";
import { compose } from "recompose";
import PropTypes from 'prop-types';
import DateFnsUtils from '@date-io/date-fns';
import FormGroup from '@material-ui/core/FormGroup';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import Checkbox from '@material-ui/core/Checkbox';
import { MuiPickersUtilsProvider, DatePicker } from 'material-ui-pickers';
import classNames from 'classnames';
import CheckCircleIcon from '@material-ui/icons/CheckCircle';
import IconButton from '@material-ui/core/IconButton';
import CloseIcon from '@material-ui/icons/Close';
import ErrorIcon from '@material-ui/icons/Error';
import InfoIcon from '@material-ui/icons/Info';
import green from '@material-ui/core/colors/green';
import amber from '@material-ui/core/colors/amber';
import SnackbarContent from '@material-ui/core/SnackbarContent';
import WarningIcon from '@material-ui/icons/Warning';

const variantIcon = {
  success: CheckCircleIcon,
  warning: WarningIcon,
  error: ErrorIcon,
  info: InfoIcon,
};

const styles1 = theme => ({
  success: {
    backgroundColor: green[600],
  },
  error: {
    backgroundColor: theme.palette.error.dark,
  },
  info: {
    backgroundColor: theme.palette.primary.dark,
  },
  warning: {
    backgroundColor: amber[700],
  },
  icon: {
    fontSize: 20,
  },
  iconVariant: {
    opacity: 0.9,
    marginRight: theme.spacing.unit,
  },
  message: {
    display: 'flex',
    alignItems: 'center',
  },
});

function MySnackbarContent(props) {
  const { classes, className, message, onClose, variant, ...other } = props;
  const Icon = variantIcon[variant];
  return (
    <SnackbarContent
      className={classNames(classes[variant], className)}
      aria-describedby="client-snackbar"
      message={
        <span id="client-snackbar" className={classes.message}>
          <Icon className={classNames(classes.icon, classes.iconVariant)} />
          {message}
        </span>
      }
      action={[
        <IconButton
          key="close"
          aria-label="Close"
          color="inherit"
          className={classes.close}
          onClick={onClose}
        >
        <CloseIcon className={classes.icon} />
        </IconButton>,
      ]}
      {...other}
    />
  );
}

MySnackbarContent.propTypes = {
  classes: PropTypes.object.isRequired,
  className: PropTypes.string,
  message: PropTypes.node,
  onClose: PropTypes.func,
  variant: PropTypes.oneOf(['success', 'warning', 'error', 'info']).isRequired,
};

const MySnackbarContentWrapper = withStyles(styles1)(MySnackbarContent);


const styles = {
  grid: {
    width: '60%',
  },
};

export class ReservationAdd extends React.Component {
  constructor(props) {
    super(props);
    var get_tabel = this.props.tableList;
    this.id = this.props.match.params.id*1;
    var dataForm = this.props.reservtionList.find(item => item.id === this.id   );
    var tbl_disabled =[];
    var find = this.props.reservtionList.filter(x => x.reserv_date === dataForm.reserv_date);
    console.log(find);
    if(find){
      find.forEach(function(reserv,index) {
        reserv.tables.forEach(function(el) {
          tbl_disabled.push(el);
        })
      });
    }
    
    console.log(dataForm);
    this.state = {
      messageopen: false,
      open:false,
      messageInfo: {},
      list: this.props.reservtionList,
      tables: {
        list: get_tabel,
        disabled : tbl_disabled
      },
      dataForm: dataForm
    }

  }
  validate() {
    var error ='';
    if(this.state.dataForm.customer<=0){
      error += "Please specify customer. ";
    }
    if(this.state.dataForm.tables.length==0){
      error += "Please choose tables. ";
    }
    if(this.state.dataForm.name.trim()==''){
      error += "Please specify name. ";
    }
    if(this.state.dataForm.tel.trim()==''){
      error += "Please specify tel. ";
    }
    
    return error;
  }
  giveSuccessMessage = (message) => {
    let newmsg = {
      message,
      key: new Date().getTime()
    };
    this.setState({ messageInfo: newmsg, open: 1 });
    setTimeout(
      function () {
        this.setState({ open: 0 });
      }
        .bind(this),
      2000
    );
  };
  giveErrorMessage = (message) => {
    let newmsg = {
      message,
      key: new Date().getTime()
    };
    this.setState({ messageInfo: newmsg, open: 2 });
    setTimeout(
      function () {
        this.setState({ open: 0 });
      }
        .bind(this),
      2000
    );

  };
  onSubmit = () => {
    console.log("editForm",this.state.dataForm);
    var error = this.validate();
    if(error =='') {
      this.props.edit(this.state.dataForm)
        this.giveSuccessMessage('Saved');
        setTimeout(
          function () {
            this.props.history.push('/reservation');
          }
            .bind(this),
          3000
        );
    }else{
      this.giveErrorMessage(error);
    }
  }
  handleChange = name => event => {
    let dataForm = Object.assign({}, this.state.dataForm);    //creating copy of object
    dataForm[name] = event.target.value;
    this.setState({ dataForm })
  };
  handleDateChange = date => {
    let dataForm =  Object.assign({}, this.state.dataForm);
    let tables = Object.assign({}, this.state.tables);;
    let dateFormat =  this.handleFormat(date);
    dataForm.reserv_date = dateFormat;
    tables.disabled = [];
    var find = this.state.list.filter(x => x.reserv_date === dateFormat);
    if(find){
      find.forEach(function(reserv,index) {
        reserv.tables.forEach(function(el) {
          tables.disabled.push(el);
        })
      });
    }
    console.log("dataForm",dataForm);
    this.setState({tables});
    this.setState({dataForm});
  };
  handleFormat(date){
    var today = date;
    var dd = String(today.getDate()).padStart(2, '0');
    var mm = String(today.getMonth() + 1).padStart(2, '0'); //January is 0!
    var yyyy = today.getFullYear();
    return  yyyy + '-' + mm + '-' + dd;
  }
  onChangeTable = (item) => event => {
    console.log(item,);
    let dataForm = Object.assign({}, this.state.dataForm);    //creating copy of object
    if ( event.target.checked == true) {
        dataForm.tables.push(item);
        this.setState({ dataForm })
    } else {
        let index = dataForm.tables.findIndex(
          n => n.id === item.id
        );
      if (index !== -1) {

        dataForm.tables.splice(index, 1);
      }
      this.setState({ dataForm })
    }
  }
  handleCheckboxDisabled =  (item) => {
  
    var checked = this.state.dataForm.tables.find(n => n.id === item.id  )
    if(checked){
        return false
    }else{
      var disabled = this.state.tables.disabled.find(n => n.id === item.id  )
      if(disabled){
          return true
      }else{
        return false
      }
    }
  
  };
  handleCheckboxChecked=  (item) => {
    var checked = this.state.dataForm.tables.find(n => n.id === item.id  )
    if(checked){
        return true
    }else{
      return false
    }
  
  };
  render() {
    const { classes } = this.props;
    const { message, key } = this.state.messageInfo;
    return (
      <div className="contact-page-wrapper" justify="center" >
        <Grid container spacing={24} justify="center" style={{ marginTop: 10 }}>
          <Grid item xs={8} style={{ margin: 0 }}>
            <Paper className="homepagepaper">
              <div className="headerForm">
                <Typography component="h2">
                  ADD FORM : RESERVATION SEAT
              </Typography>
              </div>
              <Grid item xs={12} lg={12} md={12}>
                <TextField
                  required
                  id="name"
                  fullWidth={true}
                  label="Name"
                  placeholder="Name"
                  value={this.state.dataForm.name}
                  onChange={this.handleChange('name')}
                  margin="normal"
                />
              </Grid>
              <Grid item xs={12} lg={12} md={12}>
                <TextField
                  id="email"
                  fullWidth={true}
                  label="email"
                  placeholder="Email"
                  value={this.state.dataForm.email}
                  onChange={this.handleChange('email')}
                  margin="normal"
                />
              </Grid>
              <Grid item xs={12} lg={12} md={12}>
                <TextField
                  required
                  id="tel"
                  fullWidth={true}
                  label="tel"
                  rowsMax="4"
                  placeholder="Tel"
                  value={this.state.dataForm.tel}
                  onChange={this.handleChange('tel')}
                  margin="normal"
                />
              </Grid>
              <Grid item xs={12} lg={12} md={12}>
                <TextField
                  required
                  id="customer"
                  fullWidth={true}
                  label="customer"
                  type="number"
                  placeholder="Customer"
                  value={this.state.dataForm.customer}
                  onChange={this.handleChange('customer')}
                  margin="normal"
                />
              </Grid>
              <Grid item xs={12} lg={12} md={12}>
                <MuiPickersUtilsProvider utils={DateFnsUtils}>
                  <DatePicker
                    margin="normal"
                    label="Date"
                    minDate={this.state.dataForm.reserv_date}
                    value={this.state.dataForm.reserv_date}
                    onChange={this.handleDateChange}
                  />
                  {/* <TimePicker
                    margin="normal"
                    label="Time"
                    value={this.state.dataForm.reserv_date}
                    onChange={this.handleDateChange}
                  /> */}
                </MuiPickersUtilsProvider>
              </Grid>

              <Grid item xs={12} lg={12} md={12} style={{ marginTop: 10 }} >
                <FormGroup row>
                  {this.state.tables.list.map((n, index) => (
                    <FormControlLabel key={index}
                    // {this.state.tables.disabled.indexOf(n)> -1}
                      control={
                        <Checkbox
                          checked={this.handleCheckboxChecked(n)}
                          disabled={this.handleCheckboxDisabled(n)}
                          value={n.id}
                          color="primary"
                          onChange={this.onChangeTable(n)}
                          value={n.title}
                        />
                      }
                      label={n.title} 
                    />
                  ))}
                </FormGroup>
              </Grid>

           
              <Grid item xs={8} style={{ margin: 0 }}>
                  {this.state.open == 2 ?
                    <MySnackbarContentWrapper
                      variant="error"
                      className={classes.margin}
                      message={message}
                    />
                    : null}
                  {this.state.open == 1 ?
                    <MySnackbarContentWrapper
                      variant="success"
                      className={classes.margin}
                      message={message}
                    /> : null}
                </Grid>
                {this.state.open == 0 ?
                <Grid item xs={12} lg={12} md={12} style={{ marginTop: 40 }}>
                  <Link to="/reservation">
                      <Button type="button" color="default" variant="raised" style={{ marginRight: 10 }} >Go Back</Button>
                  </Link>
                  <Button type="button" color="primary" variant="raised" onClick={this.onSubmit}>Save</Button>
                </Grid>
                 : null}
            </Paper>
          </Grid>
        </Grid>
      </div>
    );
  }

}


function mapStateToProps(state, ownProps) {
  return {
    reservtionList: state.reservtionList,
    tableList: state.tableList

  }

}
function mapDispatchToProps(dispatch,ownProps) {
  return {
    edit: (value) => {
      dispatch({ type: 'EDIT_RESERVATION', payload: value ,id:(ownProps.match.params.id*1)})
    },
  }
}

ReservationAdd.propTypes = {
  classes: PropTypes.object.isRequired,
};
export default compose(
  withStyles(styles),
  connect(
    mapStateToProps,
    mapDispatchToProps
  )
)(ReservationAdd);